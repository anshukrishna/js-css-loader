class JSCSSLoader {
	static async __loadAFile(type, path) {
		let node
		switch(type.toLowerCase()) {
			case 'js':
				node = document.createElement('script');
				node.setAttribute('src', path);
				break;
			case 'css':
				node = document.createElement('link');
				node.setAttribute('rel', 'stylesheet');
				node.setAttribute('href', path);
				break;
		}
		return await new Promise((resolve) => {
			let onload, onerror;
			onload = e => {
				e.stopPropagation();
				e.target.removeEventListener('load', onload);
				e.target.removeEventListener('error', onerror);
				resolve({
					path: path,
					loaded: true
				});
			};
			onerror = e => {
				e.stopPropagation();
				document.head.removeChild(e.target);
				// e.target.removeEventListener('load', onload);
				// e.target.removeEventListener('error', onerror);
				resolve({
					path: path,
					loaded: false
				});
			}
			node.addEventListener('load', onload);
			node.addEventListener('error', onerror);
			document.head.appendChild(node);
		});
	}
	static async loadScript(...paths) {
		let promises = paths.map(path => JSCSSLoader.__loadAFile('js', path));
		let state = await Promise.all(promises).then(b => b).catch(b => b);
		return state;
	}
	static async loadCSS(...paths) {
		let promises = paths.map(path => JSCSSLoader.__loadAFile('css', path));
		let state = await Promise.all(promises).then(b => b).catch(b => b);
		return state;
	}
	static async loadScriptSequentially(...paths) {
		let ret = [];
		for(let path of paths) {
			let state = await JSCSSLoader.__loadAFile('js', path);
			ret.push(state);
		}
		return ret;
	}
	static async loadCSSSequentially(...paths) {
		let ret = [];
		for(let path of paths) {
			let state = await JSCSSLoader.__loadAFile('css', path);
			ret.push(state);
		}
		return ret;
	}
}